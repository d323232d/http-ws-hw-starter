export const startTimer = (sec, selector, socket, isModal = false) => {
  let start = sec;
  const timerEl = document.getElementById(selector);
  timerEl.classList.remove('display-none');
  
  let timer = setInterval(() => {
    timerEl.innerText = `${--start} sec`;
    if(!isModal) {
      if(start <= 0) {
        clearInterval(timer);        
        socket.emit("START_GAME");
      } 
    } else {
      timerEl.classList.add("game__limit");
      if(start <= 0 ) {
        clearInterval(timer);
        timerEl.classList.add('display-none');
        socket.emit("TIME_IS_OVER", 'TIME_IS_OVER');
      } 
    }
    
  }, 1000);   
}