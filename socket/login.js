export let users = [];


export default io => {
  io.on("connection", socket => {
    console.log('socket login: ', socket.id);

    socket.on("LOGIN_USER", (userName) => {      
      if(users.some(item => item === userName)) {
        socket.emit("USER_EXIST", userName);
        return;
      }
      users.push(userName);
      socket.emit("LOGIN_OK", userName);
    });

    socket.on("disconnect", () => {
     
      socket.emit("SESSION_OFF");
    });
  })
}