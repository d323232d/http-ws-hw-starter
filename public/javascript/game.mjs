import { 
  createTitle, createGamersList, 
  createRoomCard 
} from './helpers.mjs';
import { startTimer } from './timer.mjs';

const username = sessionStorage.getItem("username");
console.log('username: ', username);

if (!username) {
  window.location.replace("/login");
}

const socket = io("http://localhost:3002/rooms", { query: { username } });

const roomsPageContainer = document.getElementById('rooms-page');
const gamePageContainer = document.getElementById('game-page');
const readyBtn = document.getElementById('ready-btn');
const quitRoomBtn = document.getElementById('quit-room-btn');
const createRoomBtn = document.getElementById('add-room-btn');
const roomsWrapper = document.querySelector('.rooms__wrapper');
const gameTextField = document.querySelector('.game__text');
const modalEl = document.getElementById('modal');

let activeRoomId = null;

const setActiveRoomId = roomId => {
  activeRoomId = roomId;
}

const createRoom = (roomName) => {
  roomsWrapper.insertAdjacentHTML('afterbegin', createRoomCard(roomName));
}

createRoomBtn.addEventListener('click', () => {
  let roomName = prompt("Enter room's name");
  createRoom(roomName);
  socket.emit("CREATE_ROOM", roomName);
})

const updateUsersNumber = ({usersNumber = 0, roomId}) => {
  const roomCounter = document.querySelectorAll('.room__counter');  
  roomCounter.forEach(item => {
    if(item.dataset.id === roomId) {
      item.textContent = `${usersNumber}  users connected`;
    }
  });
};


const updateRooms = (rooms) => {
  roomsWrapper.innerHTML = '';
  rooms.map(room => roomsWrapper.insertAdjacentHTML('beforeend', createRoomCard(room)));

  const onJoinClick = (roomId) => {
    if(activeRoomId === roomId) {
      return;
    }
    roomsPageContainer.classList.add('display-none');
    gamePageContainer.classList.remove('display-none');
    socket.emit("JOIN_ROOM", roomId);
    socket.emit("ADD_USER_TO_ROOMLIST");
    socket.emit("RENDER_GAME_ROOM");
  };

  const joinBtns = document.querySelectorAll('.room__join');
  joinBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      onJoinClick(btn.id);
    })
  });

};

const joinRoomDone = ({usersNumber, roomId}) => {
  const roomElement = document.getElementById(roomId);
  roomElement.classList.add('active');

  if (activeRoomId) {
    const previousRoomElement = document.getElementById(activeRoomId);
    previousRoomElement.classList.remove("active");
  }

  updateUsersNumber(usersNumber, roomId);
  setActiveRoomId(roomId);
};

const updateGameSpace = ({roomId, gamersList}) => {
  createTitle(roomId);
  createGamersList(gamersList, "users__list");
  
  const quitRoomBtn = document.getElementById('quit-room-btn');
  quitRoomBtn.addEventListener('click', () => {    
    roomsPageContainer.classList.remove('display-none');
    gamePageContainer.classList.add('display-none');

    socket.emit("USER_LEAVE_GAME", username);
    setActiveRoomId(null);    
  })
};

const updateGamersList = (gamersList) => {
  createGamersList(gamersList, "users__list");
};

readyBtn.addEventListener('click', (e) => {
  const target = e.target;
  
  if(target.textContent === 'READY') {
    console.log("click ready btn");
    target.innerText = 'NOT READY';
    socket.emit("USER_READY", username);   
  } else {
    target.innerText = 'READY';
    socket.emit("USER_NOT_READY", username);   
  };
});

const startGame = ({ sec, textId }) => {
  quitRoomBtn.style.display = 'none';
  readyBtn.style.display = 'none';
  startTimer(sec, 'timer', socket);  
};

let text = '';
let textArray ; // array from textstring;
let typedTextArray = [];

const changeProgress = (start, carr) => {  
  return (carr / start) * 100;  
};

const showText = ({ textId, sec }) => {  
  // Why not work? :( )
  // fetch(`http://localhost:3002/game/texts/${textId}`)
  //   .then(response => response.json())
  //   .then(data => text = data); //

  text = "Text for typing #1";
  let startLength = text.length;
  textArray = text.split('');
  gameTextField.innerHTML = text;
  typedTextArray = [];
  let newText;
  const userBar = document.querySelector(`.${username} .user__progressbar`);  

  document.body.addEventListener('keyup', (e) => {
    let pressKey = e.key;
    if(textArray[0] == pressKey) {      
      typedTextArray.push(textArray[0]);
      textArray.shift();
      
      newText = `<span class="mark">${typedTextArray.join('')}</span>${textArray.join('')}`;
      gameTextField.innerHTML = newText;

      let barWidth = changeProgress(startLength, typedTextArray.length);
      userBar.style.width = barWidth + '%';
      
      socket.emit('UPDATE_USER_PROGRESS', barWidth);
    }
    if(!textArray.length) {
      userBar.style.backgroundColor = 'greenyellow';
      socket.emit("GAME_OVER", username);
    }
  });
  startTimer(sec, 'timer', socket , true, textArray.length, username);
};

const showWinnerModal = (winners) => {
  console.log('GAME_OVER');
  modalEl.classList.remove('display-none');
  modalEl.classList.add('flex');
  createGamersList(winners, "modal__list");
};

const showMessage = (message) => {
  modalEl.classList.remove('display-none');
  modalEl.classList.add('flex');
  modalEl.querySelector('.modal__content h2').textContent = message;
}

socket.on("UPDATE_ROOMS", updateRooms);
socket.on("UPDATE_USER_COUNTER", updateUsersNumber);
socket.on("UPDATE_GAME_ROOM", updateGameSpace);
socket.on("UPDATE_GAMERS_LIST", updateGamersList);
socket.on("START_TIMER", startGame);
socket.on("SEND_TEXT", showText);
socket.on("SHOW_WINNERS", showWinnerModal);
socket.on("SHOW_MESSAGE", showMessage);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
// socket.on("SESSION_OFF", () => {
//   sessionStorage.clear();
// });

