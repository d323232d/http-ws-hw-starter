export const createRoomCard = roomId => {  
  const roomCard = `
    <div class="room_item room">
      <p class="room__counter" data-id="${roomId}">00 users connected</p>
      <h3 class="room__title">Room <br>"${roomId}"</h3>
      <button class="room__join flex-centered no-select" id="${roomId}">Join</button>
    </div>
  `;
  return roomCard;  
};

export const createTitle = (roomId) => {
  const roomName = document.querySelector('.room__name');
  roomName.textContent = `Room "${roomId}"`
}

export const createGamersList = (list, listContainer) => {
  const usersListContainer = document.querySelector(`.${listContainer}`);
  usersListContainer.querySelectorAll('li').forEach(el => el.remove());
  let userEl = '';
  
  list?.forEach(user => {
    let El = `
      <li class="user__info">
        <span class="${user.isReady ? "ready-status ready-status-green" : "ready-status ready-status-red"}" data-id=${user.userName}></span>
        <h3 class="user__name">${user.userName}</h3>
        <div class="user-progress ${user.userName}"><div  class="user__progressbar"  style="width:${user.progress + '%'}"></div></div>
      </li>
    `;    
    userEl += El;
  }) || '';

  usersListContainer.insertAdjacentHTML('afterbegin', userEl)
};

