import * as config from "./config";
import { users } from "./login";
import texts from '../data.js';

let textIndex = 1;
// 
let roomsList = new Map();
roomsList.set(
  'default', {
    connectedUsers: [], //{socketId: socket.id, userName: 'u1'}
    timerBeforeGameId: false
  }
);

const checkUserInRoom = (roomId, username) => {
  return roomsList.get(roomId).connectedUsers.some(user => user.userName === username);
};

const addNewRoomToList = (roomId, roomPropsValue = {
  connectedUsers: [],
  timerBeforeGameId: false
}) => {
  return roomsList.set(roomId, roomPropsValue) ;
};

const getAllRooms = () => {
  return Array.from(roomsList.keys());
}

const getRoomProps = (roomId, propString) => {
  const roomProps = roomsList.get(roomId);
  return roomProps[propString];
};

const setRoomPropsValue = (roomId, propString, newPropValue) => {
  const roomProps = roomsList.get(roomId);
  let newRoomProps = {
    ...roomProps,
    [propString]: newPropValue
  };
  return roomsList.set(roomId, newRoomProps);  
};

// setRoomPropsValue('r1', 'connectedUsers', ['den', 'fred']);
// setRoomPropsValue('r1', 'timerBeforeGameId', true);
// console.log(getRoomProps('r1', 'connectedUsers'));
// console.log(getRoomProps('r1', 'timerBeforeGameId'));
//

let connectedUsersMap = new Map();
// connectedUsersMap.set(socket.id, {userName: username, progress: 0, isReady: false, connectedRoomId: null});

const infoUsersInRoom = (roomsMap, usersMap, roomId) => {
  if(roomsMap.size) {
    const socketsInRoom = roomsMap.get(roomId)?.connectedUsers.map(userObj => userObj.socketId); //[sock, sock]
    return socketsInRoom?.map(socketId => usersMap.get(socketId)) || []; //[{},{}]
  }
  return [];  
};

const checkReadyUsers = (roomsMap, usersMap, roomId) => {
  const infoUsersInRoomArr = infoUsersInRoom (roomsMap, usersMap, roomId);
  // const socketsInRoom = roomsMap.get(roomId).connectedUsers.map(userObj => userObj.socketId); //[sock, sock]
  // const infoUsersInRoom = socketsInRoom.map(socketId => usersMap.get(socketId)); //[{},{}]
  return infoUsersInRoomArr.length ? infoUsersInRoomArr.every(user => user.isReady) : false;
};

const getCurrentRoom = socket => Object.keys(socket.rooms).find(roomId => roomsList.has(roomId));

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    // const currentRoom = getCurrentRoom(socket);
    console.log(`${username} joining to platform`);

    let rooms = getAllRooms();
    socket.emit("UPDATE_ROOMS", rooms);

    socket.on("CREATE_ROOM", newRoomId => {
      addNewRoomToList(newRoomId);
      let rooms =getAllRooms();

      io.emit("UPDATE_ROOMS", rooms);
    })

    socket.on("JOIN_ROOM", roomId => {
      const prevRoomId = getCurrentRoom(socket);
      if (roomId === prevRoomId) return;
      if (prevRoomId) {
        socket.leave(prevRoomId);
      }

      socket.join(roomId, () => {
        connectedUsersMap.set(socket.id, {
          userName: username,
          isReady: false,
          connectedRoomId: roomId,
          progress: 0
        });
        let users = getRoomProps(roomId, 'connectedUsers');
        io.to(socket.id).emit("JOIN_ROOM_DONE", { usersNumber: users.length, roomId})
      });

      io.in(roomId).emit("UPDATE_GAMERS_LIST", users);
      io.emit("UPDATE_USER_COUNTER", {
        usersNumber: connectedUsersMap.size,        
        roomId: roomId
      });
    });

    socket.on("ADD_USER_TO_ROOMLIST", () => {    
      const currentRoom = getCurrentRoom(socket);
      let users = getRoomProps(currentRoom, 'connectedUsers');      
      users.push({socketId: socket.id, userName: username});
      
      setRoomPropsValue(currentRoom, 'connectedUsers', users);

      io.emit("UPDATE_USER_COUNTER", {
        usersNumber: users.length,
        roomId: currentRoom
      });
    });

    socket.on("RENDER_GAME_ROOM", () => {
      const currentRoom = getCurrentRoom(socket);
      let users = getRoomProps(currentRoom, 'connectedUsers');
      io.to(currentRoom).emit("UPDATE_GAME_ROOM", {
        roomId: currentRoom,
        gamersList: users
      }); 
      // socket.to(currentRoom).emit("UPDATE_GAMERS_LIST", users);
      io.emit("UPDATE_USER_COUNTER", {
        usersNumber: users.length,
        roomId: currentRoom
      });
    })

    socket.on("USER_LEAVE_GAME", (username) => {
      const currentRoom = getCurrentRoom(socket);
      let updUsers =roomsList.get(currentRoom).connectedUsers.filter(user => user.userName !== username);
      
      setRoomPropsValue(currentRoom, 'connectedUsers', updUsers);
      io.in(currentRoom).emit("UPDATE_GAMERS_LIST", updUsers);
      
      io.emit("UPDATE_USER_COUNTER", {
        usersNumber: users.length,
        roomId: currentRoom,
      });
    });    

    socket.on("USER_READY", username => {
      connectedUsersMap.set(socket.id, {
        ...connectedUsersMap.get(socket.id),
        isReady: true
      });
      const currentRoom = getCurrentRoom(socket);
      let users = infoUsersInRoom(roomsList, connectedUsersMap, currentRoom)
      
      io.to(currentRoom).emit("UPDATE_GAMERS_LIST", users);
      
      let usersReady = checkReadyUsers(roomsList, connectedUsersMap, currentRoom);      
      const usersNumberInRoom = roomsList.get(currentRoom).connectedUsers.length;
      
      if(usersNumberInRoom < 2 ) return;

      if(usersReady) {
        const currentRoom = getCurrentRoom(socket);
        io.in(currentRoom).emit("START_TIMER", {
          sec: config.SECONDS_TIMER_BEFORE_START_GAME,
          textId: 1
        });
      }
    });

    socket.on("USER_NOT_READY", username => {
      connectedUsersMap.set(socket.id, {
        ...connectedUsersMap.get(socket.id),
        isReady: false
      });
      const currentRoom = getCurrentRoom(socket);      
      let users = infoUsersInRoom(roomsList, connectedUsersMap, currentRoom)
            
      io.to(currentRoom).emit("UPDATE_GAMERS_LIST", users);
    });

    socket.on("START_GAME", () => {
      const currentRoom = getCurrentRoom(socket);  
      io.to(currentRoom).emit("SEND_TEXT", {
        textIndex,
        sec: config.SECONDS_FOR_GAME
      });
      
      if(textIndex = texts.length - 1) {
        textIndex = 0;
      } else {
        textIndex ++;
      }
    });

    socket.on('UPDATE_USER_PROGRESS', barWidth => {
      connectedUsersMap.set(socket.id, {
        ...connectedUsersMap.get(socket.id),
        progress: barWidth
      });
      const currentRoom = getCurrentRoom(socket);
      let users = infoUsersInRoom(roomsList, connectedUsersMap, currentRoom);

      io.to(currentRoom).emit("UPDATE_GAMERS_LIST", users);
    });

    socket.on("GAME_OVER", (username) => {
      const currentRoom = getCurrentRoom(socket);
      let users = infoUsersInRoom(roomsList, connectedUsersMap, currentRoom)
      let winners = users.filter(user => user.userName === username);
      // console.log('winners: ', winners);
      io.to(currentRoom).emit("SHOW_WINNERS", winners);

    });

    socket.on("TIME_IS_OVER", (message) => {
      const currentRoom = getCurrentRoom(socket);
      io.to(currentRoom).emit("SHOW_MESSAGE", message);
    });    

    socket.on("disconnect", () => {
      console.log(`${username} has left game`);           
    });
  });

  
};
