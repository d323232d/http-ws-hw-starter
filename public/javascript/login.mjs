const username = sessionStorage.getItem("username");

if (username) {
  window.location.replace("/game");
}

const socket = io("http://localhost:3002/login");

const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  console.log('inputValue: ', inputValue);
  if (!inputValue) {
    return;
  }
  if(inputValue === username) {
    return ;
  } else {
    socket.emit("LOGIN_USER", inputValue);
  }

};

const signingIn = (username) => {
  sessionStorage.setItem("username", username);
  window.location.replace("/game");
}

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};



submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);

socket.on("USER_EXIST", (username) => alert(`User with name ${username} already exist. Please, enter other name`))
socket.on("LOGIN_OK", signingIn);
socket.on("SESSION_OFF", () => {
  sessionStorage.clear();
});
