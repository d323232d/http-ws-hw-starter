// ------------------------
// Все комнаты
let roomsList = new Map();
Map {
  'default', {
    connectedUsers: ['u1', 'u2'],
    connectedUsers: [
      {socketId: socket.id, userName: 'u1'}, 
      {socketId: socket.id, userName: 'u2'}
    ],

    timerBeforeGameId: false
  }
}
// ------------------------
// Все юзеры
let connectedUsersMap = new Map();

Map {
  socket.id, {
    userName: 'u1',
    isReady: false, // onClick ready-btn -> true
    connectedRoomId: null // onClick join-btn -> roomId
  },
  socket.id, {
    userName: 'u2',
    isReady: false,
    connectedRoomId: null
  },
  socket.id, {
    userName: 'u3',
    isReady: false,
    connectedRoomId: null
  },
}


'u1': {
  socketId: socket.id,
  isReady: false, 
  connectedRoomId: null 
}